using FluentAssertions;
using System;
using Xunit;

namespace CuentraLetras.XUnitTests;

public class ContadorTests
{
    public ContadorTests()
    {
        Console.WriteLine("Nueva instancia de ContadorTests");
    }
    
    [Fact]
    public void Should_Return_Zero_When_Empty_Word()
    {
        // Given
        var word = string.Empty;
        var letter = 'A';
        var expectedResult = 0;

        // When
        var result = Contador.GetCharCount(word, letter);
        
        // Then
        Assert.Equal(expectedResult, result);
    }

    [Fact]
    public void Should_Return_Zero_When_Word_Does_Not_Contain_Char()
    {
        var result = Contador.GetCharCount("sample", 'A');
        Assert.Equal(0, result);
    }

    [Theory]
    [InlineData("hola", 'o', 1)]
    [InlineData("adios", 'a', 1)]
    [InlineData("aprendiendo a testear con .NET", 'a', 3)]
    public void Should_Return_The_Expected_Number_Of_Characters_Found(
        string inputWord, 
        char inputChar, 
        int expectedResult)
    {
        // When
        var result = Contador.GetCharCount(inputWord, inputChar);
        
        // Then
        Assert.Equal(expectedResult, result);
    }
    
    [Theory]
    [InlineData("hola", 'o', 1)]
    [InlineData("adios", 'a', 1)]
    [InlineData("aprendiendo a testear con .NET", 'a', 3)]
    public void Should_Return_The_Expected_Number_Of_Characters_Found_With_FluentAssertions(
        string inputWord, 
        char inputChar, 
        int expectedResult)
    {
        // When
        var result = Contador.GetCharCount(inputWord, inputChar);
        
        // Then
        //Assert.Equal(expectedResult, result);
        result.Should().Be(expectedResult);
    }
}