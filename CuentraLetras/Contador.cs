namespace CuentraLetras;

public static class Contador
{
    public static int GetCharCount(string word, char letter)
    {
        var result = word.Count(x => x == letter);
        return result;
    }
}