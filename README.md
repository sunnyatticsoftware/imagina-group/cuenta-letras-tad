# cuenta-letras

Ejemplo con Test After Development (TAD)


## Ejercicio
Crear el CuentraLetras con un Contador
```
public static class Contador
{
    public static int GetCharCount(string word, char letter)
    {
        var result = word.Count(x => x == letter);
        return result;
    }
}
```

Crea tests en xUnit y nUnit, compara la sintaxis.

Ahora genera un reporte de la ejecución de los tests.